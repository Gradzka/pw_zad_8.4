// Zad_8.4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <iostream>

volatile double start;
volatile double stop;

#define num_of_meas 10

double silnia(int n)
{
	double silnia = 1.0;
	double i;
	for (i = 1.0; i <= n; i++)
	{
		silnia = silnia*i;
	}
	return silnia;
}
double test1(int num_steps)
{
	int i;
	double x = 0.0;
	double e = 0.0;
	for (i = 0; i < num_steps; i++)
	{
		x = 1 / silnia(i);
		e = e + x;
	}

	return e;
}
double test2(int num_steps)
{
	int i;
	double x = 0.0;
	double e = 0.0;

#pragma omp parallel for reduction(+:e) private(x)
	for (i = 0; i < num_steps; i++)
	{
		x = 1 / silnia(i);
		e = e + x;
	}
	return e;
}
int main(int argc, char* argv[])
{
	FILE* plik;
	double d;
	double result_s, result_p;
	double min_time_s, min_time_p, max_time_s, max_time_p, avg_time_s, avg_time_p;
	start = stop = result_s = result_p = min_time_s = min_time_p = max_time_s = max_time_s = avg_time_s = avg_time_p = 0;

	int i, n = 40000;
	fopen_s(&plik, "pomiary.txt", "w");

	// printf("Liczba dostepnych rdzeni %d\n", omp_get_num_procs());
	// omp_set_num_threads(2);
	fprintf(plik, "Pomiary wykonano dla %d krokow: \n\n", n);
	//printf("Pomiary wykonano dla %d krokow: \n\n", n);
	printf("Rozpoczeto zapis pomiarow do pliku. Czekaj na komunikat o zakonczeniu!\n");
	if (argc > 1) n = atoi(argv[1]);

	for (i = 1; i <= num_of_meas; i++)
	{
		fprintf(plik, "Pomiar %d: \n", i);
		//printf("Pomiar %d: \n", i);
		start = omp_get_wtime();
		d = test1(n);
		stop = omp_get_wtime();
		result_s = stop - start;
		avg_time_s += result_s;
		if (i == 1) { min_time_s = result_s; max_time_s = result_s; }
		else
		{
			if (result_s < min_time_s) { min_time_s = result_s; }
			if (result_s > max_time_s) { max_time_s = result_s; }
		}
		//printf_s("S: pi = %.15f, time: %f seconds\n", d, result_s);
		fprintf(plik, "S: e = %.15f, time: %f seconds\n", d, result_s);
		start = omp_get_wtime();
		d = test2(n);
		stop = omp_get_wtime();
		result_p = stop - start;
		avg_time_p += result_p;
		if (i == 1) { min_time_p = result_p; max_time_p = result_p; }
		else
		{
			if (result_p < min_time_p) { min_time_p = result_p; }
			if (result_p>max_time_p) { max_time_p = result_p; }
		}
		//printf_s("P: pi = %.15f, time: %f seconds\nRatio_acc: %f\n\n", d, result_p,result_s/result_p);
		fprintf(plik, "P: e = %.15f, time: %f seconds\nRatio_acc: %f\n\n", d, result_p, result_s / result_p);
	}
	fprintf(plik, "Minimalny i maksymalny czas obliczania liczby e sekwencyjnie i wspolbieznie:\n");
	fprintf(plik, "S: min time: %f seconds and max time: %f seconds\n", min_time_s, max_time_s);
	fprintf(plik, "P: min time: %f seconds and max time: %f seconds\n", min_time_p, max_time_p);

	fprintf(plik, "\nSredni czas obliczania liczby e sekwencyjnie i wspolbieznie:\n");
	fprintf(plik, "S: avg time: %f seconds\n", (avg_time_s / num_of_meas));
	fprintf(plik, "P: avg time: %f seconds\n", (avg_time_p / num_of_meas));
	fprintf(plik, "Avg_ratio_acc: %f", ((avg_time_s / num_of_meas) / (avg_time_p / num_of_meas)));
	fclose(plik);
	printf("Zakonczono zapis pomiarow do pliku!\n");
	system("pause");
}
